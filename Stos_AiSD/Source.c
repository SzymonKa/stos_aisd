#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

typedef struct Stoss{

	char dane;
	struct Stoss *poprzedni;

}Stos;

typedef struct Stoss_Postfix{

	float dane;
	struct Stoss_Postfix *poprzedni;

}Stos_Postfix;



Stos* Stos_init(){
	Stos *nowy_stos = (Stos*)calloc(1, sizeof(Stos));

	nowy_stos->poprzedni = NULL;
	nowy_stos->dane = '\0';

	return nowy_stos;
}

Stos_Postfix* Stos_Postfix_init(){
	Stos_Postfix *nowy_stos = (Stos_Postfix*)calloc(1, sizeof(Stos_Postfix));

	nowy_stos->poprzedni = NULL;
	nowy_stos->dane = '\0';

	return nowy_stos;
}



void push(Stos **st, char c){
	Stos *ph;
	Stos *pt;

	if (((*st)->dane == '\0') || ((*st)->dane == ' ')){
		(*st)->dane = c;
	}
	else {

		ph = *st;
		pt = (Stos*)calloc(1, sizeof(Stos));
		*st = pt;
		(*st)->dane = c;
		(*st)->poprzedni = ph;

	}

}

void push_postfix(Stos_Postfix **st, float f){
	Stos_Postfix *ph;
	Stos_Postfix *pt;

	if ((((*st)->dane == '\0') || ((*st)->dane == ' '))){
		(*st)->dane = f;
	}
	else {

		ph = *st;
		pt = (Stos_Postfix*)calloc(1, sizeof(Stos_Postfix));
		*st = pt;
		(*st)->dane = f;
		(*st)->poprzedni = ph;

	}

}

void pisz_stos(Stos *st){

	if (((st == NULL) || (st->dane == '\0') || (st->dane == ' '))){
		printf("Stos Pusty \n\n",'\n');
	}
	else{

		while (st->poprzedni != NULL){
			printf("%c", st->dane);
			st = st->poprzedni;
		}
		printf("%c", st->dane);
	}

}

void pisz_stos_postfix(Stos_Postfix *st){

	if (((st == NULL) || (st->dane == '\0') || (st->dane == ' '))){
		printf("Stos Pusty \n\n", '\n');
	}
	else{

		while (st->poprzedni != NULL){
			printf("%c", st->dane);
			st = st->poprzedni;
		}
		printf("%c", st->dane);
	}

}

char pop(Stos **st){
	char c;

	if ((*st)->poprzedni != NULL){
		Stos *ph = NULL;
		c = (*st)->dane;
		ph = (*st)->poprzedni;
		free(*st);
		*st = ph;
		return c;
	}
	else{
		c = (*st)->dane;
		(*st)->dane = '\0';
		return c;
	}

}

float pop_postfix(Stos_Postfix **st){
	float f;

	if ((*st)->poprzedni != NULL){
		Stos_Postfix *ph = NULL;
		f = (*st)->dane;
		ph = (*st)->poprzedni;
		free(*st);
		*st = ph;
		return f;
	}
	else{
		f = (*st)->dane;
		(*st)->dane = '\0';
		return f;
	}

}

char peek(Stos *st){
	char c;

	if (((st->dane == '\0') || (st->dane == ' '))){
		return '@';
	}
	else{
		c = st->dane;
		return c;
	}
}

float peek_postfix(Stos_Postfix *st){
	float f;

	if (((st->dane == '\0') || (st->dane == ' '))){
		return ' ';
	}
	else{
		f = st->dane;
		return f;
	}

}

bool isEmpty(Stos *st){
	int i = 0;

	if (((st->dane != '\0') && (st->dane != ' '))){
		i++;
	}

	if (i > 0){
		return false;
	}
	else{
		return true;
	}
}

bool isEmpty_postfix(Stos_Postfix *st){
	int i = 0;

	if (((st->dane != '\0') && (st->dane != ' '))){
		i++;
	}

	if (i > 0){
		return false;
	}
	else{
		return true;
	}
}

size_t size(Stos *st){
	rsize_t i = 0;

	if ((((st->dane == '\0') || (st->dane == ' ')) && (st->poprzedni == NULL)))
	{
		return i;
	}

	while (st->poprzedni != NULL)
	{
		i++;
		st = st->poprzedni;
	} 

	i++;

	return i;
}

size_t size_postfix(Stos_Postfix *st){
	rsize_t i = 0;

	if ((((st->dane == '\0') || (st->dane == ' ')) && (st->poprzedni == NULL))) {
		return i;
	}
	
	while (st->poprzedni != NULL){
		i++;
		st = st->poprzedni;
	}

	i++;

	return i;
}

void niszcz(Stos **st){
	Stos *ph;

	while ((*st)->poprzedni != NULL)
	{
		ph = (*st)->poprzedni;
		free(*st);
		*st = ph;
	}

	if ((*st)->poprzedni == NULL)
	{
		(*st)->dane = '\0';
	}

}

void niszcz_postfix(Stos_Postfix **st){
	Stos_Postfix *ph;

	while ((*st)->poprzedni != NULL){
		ph = (*st)->poprzedni;
		free(*st);
		*st = ph;
	}

	if ((*st)->poprzedni == NULL)
	{
		(*st)->dane = '\0';
	}

}

main(){
	char symbole[] = { '*', '/', '+', '-', '(', ')' };
	char wej[] = { '2', '*', '(', '3', '+', '4', ')', '/', '4' };
	size_t wej_size = sizeof(wej)/sizeof(char);
	char *wyj = calloc((wej_size + 1), sizeof(char));
	int licz=0, n = 0,z;
	char spr, czy, a;
	size_t k = 0, e = 0;

	Stos *stos_symboli = NULL;
	stos_symboli = Stos_init();



	for (size_t i = 0; i < wej_size; i++){
		for (size_t j = 0; j < (sizeof(symbole) / sizeof(char)); j++){
			if (wej[i] == symbole[j]){


				if ((peek(stos_symboli) != '@') && (peek(stos_symboli) == '*') && (wej[i] == '+')){
					wyj[k] = pop(&stos_symboli);
					k++;
					while ((peek(stos_symboli) != '@')){
						wyj[k] = pop(&stos_symboli);
						k++;
					}
				}

				if ((peek(stos_symboli) != '@') && (peek(stos_symboli) == '/') && (wej[i] == '+')){
					wyj[k] = pop(&stos_symboli);
					k++;
					while ((peek(stos_symboli) != '@')){
						wyj[k] = pop(&stos_symboli);
						k++;
					}
				}

				if ((peek(stos_symboli) != '@') && (peek(stos_symboli) == '*') && (wej[i] == '-')){
					wyj[k] = pop(&stos_symboli);
					k++;
					while ((peek(stos_symboli) != '@')){
						wyj[k] = pop(&stos_symboli);
						k++;
					}
				}

				if ((peek(stos_symboli) != '@') && (peek(stos_symboli) == '/') && (wej[i] == '-')){
					wyj[k] = pop(&stos_symboli);
					k++;
					while ((peek(stos_symboli) != '@')){
						wyj[k] = pop(&stos_symboli);
						k++;
					}
				}

				if (wej[i] == ')'){
					e++;
					while (((peek(stos_symboli)) != '(') && ((peek(stos_symboli)) != '@')){
						wyj[k] = pop(&stos_symboli);
						k++;
					}
				}

				if (e == 1){
					czy = pop(&stos_symboli);
					while ((peek(stos_symboli) != '@')){
					
						while (peek(stos_symboli) != '@' && ((peek(stos_symboli) == '(') || ((peek(stos_symboli) == ')')))){
							czy = pop(&stos_symboli);
							n++;
						}
						if (n >= 2) break;

						if ((peek(stos_symboli) != '@')){
							wyj[k] = pop(&stos_symboli);
							k++;
						}
					}
					e = 0;
				}
								
				if (wej[i] != ')'){
					push(&stos_symboli, wej[i]);
				}
				licz++;

				break;
			}

		}
		if (licz == 0){
			wyj[k] = wej[i];
			k++;
		}
		licz = 0;
		z = i;
	}
	
	while (( peek(stos_symboli) == ')') || ( peek(stos_symboli)  == '(')){
		a = pop(&stos_symboli);
	}
	

	if (!(isEmpty(stos_symboli))){
		size_t size_stos_symb = size(stos_symboli);
		size_t wyj_size = strlen(wyj);

		for (size_t i = wyj_size; i < wej_size; i++){
			wyj[i] = pop(&stos_symboli);
		}

	}

	free(stos_symboli);

	printf("\n\nNotacja Postfix : %s\n\n", wyj);
	size_t wyj_size2 = strlen(wyj);
	Stos_Postfix *stos_liczb = NULL;

	stos_liczb = Stos_Postfix_init();
			
	int c = 0;

	for (size_t i = 0; i < wyj_size2; i++){

		for (size_t j = 0; j < (sizeof(symbole) / sizeof(char)); j++){

			if (wyj[i] == symbole[j]){
				switch (j){

					case 0:
					{ // '*'
						float a = (pop_postfix(&stos_liczb)) * (pop_postfix(&stos_liczb));
						push_postfix(&stos_liczb, a);
						c++;
						break;
					}
					case 1:
					{ // '/'
						float a = pop_postfix(&stos_liczb);
						float b = pop_postfix(&stos_liczb);					
						push_postfix(&stos_liczb, (b / a));
						c++;
						break;
					}
					case 2:
					{ // '+'
						float a = (pop_postfix(&stos_liczb)) + (pop_postfix(&stos_liczb));
						push_postfix(&stos_liczb, a);
						c++;
						break;
					}
					case 3:
					{ // '-'
						float a = pop_postfix(&stos_liczb);
						float b = pop_postfix(&stos_liczb);
						push_postfix(&stos_liczb, (b - a));
						c++;
						break;
					}

					default:{
						break;
					}
				}
								
			}
						
		}
		if (c != 0){
			c = 0; 
		}
		else{			
			push_postfix(&stos_liczb, (wyj[i] - '0'));
		}
	}



	printf("Wynik: %.3f\n\n", (pop_postfix(&stos_liczb)));

	free(stos_liczb);

	system("pause");

	return 0;
}